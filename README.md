# TTS GUI

## Instructions on integrating service demo into ELG service 'try out' tab

### Overview

ELG portal uses Angular material as UI framework to keep the interface elements consistent across the portal. This interface library is based on the idea of theming - single entry file that collects all the variables this way eliminating the need to look up for UI changes in multiple places of the project. More on Material can be found [material.io](https://material.io)
The same approach should be used for the ELG service GUI that is exposed through "try out" tab.
Angular and service GUI are using different material libraries, but ELG portal is sharing theme part for service GUI to use.


## 1. Including Material libraries

There are a couple of  files that should be included in the GUI html to get the generic Material look-and-feel.

```html
<!-- Required styles for MDC Web -->
<link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
<!-- A default font used for ELG portal -->
<link href="https://fonts.googleapis.com/css?family=Noto+Sans&display=swap" rel="stylesheet">  
<!-- Required MDC Web JavaScript library -->
<script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>         
```        

With these libraries UI will have default Material element look and the elements will work with default effects. For elements to have ELG theme, a css file, that is hosted on ELG portal and passed to the service GUI with messaging events.



Function that add css to the head of document, if proper url passed:

```javascript
let addCssToDocument = function(Uri){
      let link = document.createElement( "link" );
      link.href = Uri;
      link.type = "text/css";
      link.rel = "stylesheet";
      link.media = "screen,print";
      document.getElementsByTagName( "head" )[0].appendChild( link );
}
```

## 2. receiving configuration from elg platform

Parent container and iframe source can communicate via 'message' events. This is the way how GUI form receives configuration from ELG platform. 
Example of configuration object:

```javascript
{
  ServiceUrl: "https://dev.european-language-grid.eu/tts/", 
  StyleCss: "https://dev.european-language-grid.eu/servicegui.css", 
}
```

## 3. communication to-from iframe

Example of listening for message object from ELG portal and receiving configuration from there:

```javascript
  // Listen to messages from parent window
  window.addEventListener('message', function(e) {
      if(e.data != ''){
          //parsing data as JSON object
          serviceInfo = JSON.parse(e.data);
          // using received data to add theme css
          addCssToDocument(serviceInfo.StyleCss);
      }
  });
```

There is an option to send data from GUI to ELG platform, although there is no use case for such need at the moment:
``` javascript
    // A way how to send info to ELG platform.
  $("#send-button").on("click", function(e){
      e.preventDefault();
      window.parent.postMessage("Message from iframe", '');
  });
```



## 4. applying Material theme to the interface elements

Interface elements are created following Material best practices.

Example for Input field:
```html
<div class="mdc-text-field mdc-text-field--outlined" data-mdc-auto-init="MDCTextField">
    <input type="text" name="language" id="lang" class="mdc-text-field__input">
    <div class="mdc-notched-outline">
        <div class="mdc-notched-outline__leading"></div>
        <div class="mdc-notched-outline__notch">
            <label for="lang" class="mdc-floating-label">Language</label>
        </div>
        <div class="mdc-notched-outline__trailing"></div>
    </div>
</div>
```

Example for texarea:
```html
<div class="mdc-text-field mdc-text-field--textarea" data-mdc-auto-init="MDCTextField">
  <textarea id="text-to-speak" class="mdc-text-field__input" rows="3" cols="30"></textarea>
  <div class="mdc-notched-outline">
    <div class="mdc-notched-outline__leading"></div>
    <div class="mdc-notched-outline__notch">
      <label for="text-to-speak" class="mdc-floating-label">Type text to speak</label>
    </div>
    <div class="mdc-notched-outline__trailing"></div>
  </div>
</div>
```

Example of button:
```html
<button id="submit-form" class="mdc-button mdc-button--raised next">
  <span class="mdc-button__label">
          Submit
  </span>
</button>
```


## 5. instantiating material fields

For interface elemets to pick up Material framework, they need to be marked with tag data-mdc-auto-init="MDCTextField"

```html
 <div class="mdc-text-field mdc-text-field--textarea" data-mdc-auto-init="MDCTextField">
```

and as soon as Material libraries are loaded, javascript command should be run to instantiate interface elements:

```javascript
mdc.autoInit();
```
